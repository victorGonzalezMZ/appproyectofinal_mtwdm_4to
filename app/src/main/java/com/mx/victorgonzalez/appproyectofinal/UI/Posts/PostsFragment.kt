package com.mx.victorgonzalez.appproyectofinal.UI.Posts

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.mx.victorgonzalez.appproyectofinal.Core.Resource
import com.mx.victorgonzalez.appproyectofinal.Data.Local.AppDatabase
import com.mx.victorgonzalez.appproyectofinal.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appproyectofinal.Data.Remote.ApiClient
import com.mx.victorgonzalez.appproyectofinal.Data.Remote.PostDataSource
import com.mx.victorgonzalez.appproyectofinal.Presentation.PostViewModel
import com.mx.victorgonzalez.appproyectofinal.Presentation.PostViewModelFactory
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.Repository.PostRepositoryImp
import com.mx.victorgonzalez.appproyectofinal.UI.Adapters.PostAdapter
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentPostsBinding
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.firestore.FirebaseFirestore

class PostsFragment : Fragment(R.layout.fragment_posts) {
    private lateinit var binding : FragmentPostsBinding
    private lateinit var adapter : PostAdapter

    private val args by navArgs<PostsFragmentArgs>()

    private val viewModel by viewModels<PostViewModel> {
        PostViewModelFactory(
            PostRepositoryImp(
                LocalDataSource(AppDatabase.getDataBase(this.requireContext()).PostDao()),
                PostDataSource(ApiClient.service)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPostsBinding.bind(view)

        binding.recyclerPosts.layoutManager = GridLayoutManager(requireContext(), 2)

        onLoading()

        Log.i("V", "Key: " + args.userid)

        binding.swipeRefreshPosts.setOnRefreshListener {
            onLoading()
        }

        binding.fabSeeProfile.setOnClickListener {
            val action = PostsFragmentDirections.actionPostsFragmentToProfileDetailFragment(
                args.userid
            )
            findNavController().navigate(action)
        }

        binding.btnNewPost.setOnClickListener {

            val action = PostsFragmentDirections.actionPostsFragmentToPostEditFragment(
                args.userid,
            )
            findNavController().navigate(action)
        }
    }

    private fun onLoading(){
        viewModel.fetchPosts().observe(viewLifecycleOwner, { result ->
            when (result) {
                is Resource.Loading -> {
                    //binding.progressbar.visibility = View.VISIBLE
                    binding.swipeRefreshPosts.isRefreshing = true
                }
                is Resource.Success -> {
                    //binding.progressbar.visibility = View.GONE
                    adapter = PostAdapter(result.data.data){}
                    binding.recyclerPosts.adapter = adapter
                    binding.swipeRefreshPosts.isRefreshing = false
                    Log.i("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    //binding.progressbar.visibility = View.GONE
                    binding.swipeRefreshPosts.isRefreshing = false
                    Log.e("LiveData", "${result.exception.toString()}")
                }
            }
        })
    }

}