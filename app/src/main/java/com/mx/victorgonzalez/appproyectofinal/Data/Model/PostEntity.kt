package com.mx.victorgonzalez.appproyectofinal.Data.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.type.Date
import com.google.type.DateTime

@Entity(tableName = "postEntity")
data class PostEntity (
    @PrimaryKey
    val id          : Int    = 0,
    @ColumnInfo(name = "content")
    val content     : String = "",
    @ColumnInfo(name = "date_posted")
    val date_posted : String = "",
    @ColumnInfo(name = "userid")
    val userid      : String = ""
)