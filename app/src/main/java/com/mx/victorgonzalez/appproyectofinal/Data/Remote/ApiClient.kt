package com.mx.victorgonzalez.appproyectofinal.Data.Remote

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.mx.victorgonzalez.appproyectofinal.App.Constants

object ApiClient {
    val service by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.BaseUrl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(ApiService::class.java)
    }
}