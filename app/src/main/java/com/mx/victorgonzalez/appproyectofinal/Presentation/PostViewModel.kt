package com.mx.victorgonzalez.appproyectofinal.Presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.mx.victorgonzalez.appproyectofinal.Core.Resource
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Repository.PostRepository
import kotlinx.coroutines.Dispatchers

class PostViewModel(private val repository: PostRepository) : ViewModel() {
    fun fetchPosts() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getPosts()))
        }
        catch(e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun savePost(post: Post?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.savePost(post)))
        }
        catch(e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class PostViewModelFactory(private val repository: PostRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(PostRepository::class.java).newInstance(repository)
    }
}
