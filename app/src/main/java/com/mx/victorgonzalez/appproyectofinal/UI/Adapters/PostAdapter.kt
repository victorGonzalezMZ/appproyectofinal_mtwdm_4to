package com.mx.victorgonzalez.appproyectofinal.UI.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.databinding.ItemPostBinding
import com.squareup.picasso.Picasso

class PostAdapter(private val post: List<Post>, private val listener:(Post) -> Unit) :
    RecyclerView.Adapter<PostAdapter.viewHolder>() {

    val db = FirebaseFirestore.getInstance()

    class viewHolder(v: View) : RecyclerView.ViewHolder(v){
        val binding = ItemPostBinding.bind(v)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return viewHolder(v)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val post = post[position]

        holder.itemView.setOnClickListener {
            listener(post)
        }


        holder.binding.textPost.text     = post.content
        holder.binding.textDatePost.text = post.date_posted

        db.collection("users")
            .whereEqualTo("username", post.userid)
            .get()
            .addOnSuccessListener { result ->
                for(document in result){
                    holder.binding.textPostUsername.text = document.getString("username")
                        Picasso.get().load(document.getString("profilepic")).into(holder.binding.imgDog)
                }
            }
            .addOnFailureListener { exception ->
                Log.e("Tag","Fail")
            }
    }

    override fun getItemCount(): Int = post.size

}