package com.mx.victorgonzalez.appproyectofinal.Data.Local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostEntity

@Dao
interface PostDao {
    @Query("SELECT * FROM postEntity")
    suspend fun getPosts(): List<PostEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePost(post: PostEntity)

}