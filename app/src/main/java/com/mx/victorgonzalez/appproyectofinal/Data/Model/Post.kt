package com.mx.victorgonzalez.appproyectofinal.Data.Model

data class Post(
    val id          : Int = 0,
    val content     : String = "",
    val date_posted : String = "",
    val userid      : String = ""
)
