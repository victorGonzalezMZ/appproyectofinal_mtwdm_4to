package com.mx.victorgonzalez.appproyectofinal.UI.ProfileDetails

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Users
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.UI.Posts.PostsFragmentArgs
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentProfileEditBinding
import com.squareup.picasso.Picasso

class ProfileEditFragment : Fragment(R.layout.fragment_profile_edit) {
    private lateinit var binding: FragmentProfileEditBinding

    private val args by navArgs<PostsFragmentArgs>()

    val db = FirebaseFirestore.getInstance()

    var pass = ""
    var id = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProfileEditBinding.bind(view)

        db.collection("users")
            .whereEqualTo("username",args.userid)
            .get()
            .addOnSuccessListener { result ->
                for(document in result){
                    binding.etxtUsernameNew.setText(document.getString("username"))
                    binding.etxtEmailNew.setText(document.getString("email"))
                    binding.extDescriptionNew.setText(document.getString("desc"))
                    Picasso.get().load(document.getString("profilepic")).into(binding.imgvNewProfile)
                    pass = document.getString("password").toString()
                    id = document.id
                    Log.i("TAG", "id: " + id)
                }
            }
            .addOnFailureListener { exception ->
                Log.e("Tag","Fail")
            }

        binding.btnSaveProfileEdit.setOnClickListener {
            binding.textView1.setTextColor(Color.parseColor("#000000"))
            binding.textView2.setTextColor(Color.parseColor("#000000"))
            binding.textView5.setTextColor(Color.parseColor("#000000"))

            if (!binding.etxtUsernameNew.text.toString().isNullOrEmpty() &&
                !binding.etxtEmailNew.text.toString().isNullOrEmpty() &&
                !binding.etxtPassword.text.toString().isNullOrEmpty()){
                if(binding.etxtPassword.text.toString().equals(pass)){
                    db.collection("users")
                        .document(id)
                        .set(
                            Users(
                                username   = binding.etxtUsernameNew.text.toString(),
                                email      = binding.etxtEmailNew.text.toString(),
                                desc       = binding.extDescriptionNew.text.toString(),
                                profilepic = "https://www.nicepng.com/png/detail/128-1280406_view-user-icon-png-user-circle-icon-png.png",
                                password   = pass
                            )
                        )
                        .addOnSuccessListener {
                            Snackbar.make(requireView(),R.string.textUpdateSuccess, Snackbar.LENGTH_LONG).show();
                            findNavController().popBackStack()
                        }
                        .addOnFailureListener { exception ->
                            Snackbar.make(
                                requireView(),
                                R.string.textUpdateError.toString() + " " + exception.message.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        }
                }
                else{
                    Snackbar.make(requireView(),R.string.textProfilePasswordFail, Snackbar.LENGTH_LONG).show();
                    binding.textView5.setTextColor(Color.parseColor("#FF0000"))
                }
            }
            else {
                Snackbar.make(requireView(),R.string.textProfileEditMissing, Snackbar.LENGTH_LONG).show();
                binding.textView1.setTextColor(Color.parseColor("#FF0000"))
                binding.textView2.setTextColor(Color.parseColor("#FF0000"))
                binding.textView5.setTextColor(Color.parseColor("#FF0000"))
            }
        }
    }
}