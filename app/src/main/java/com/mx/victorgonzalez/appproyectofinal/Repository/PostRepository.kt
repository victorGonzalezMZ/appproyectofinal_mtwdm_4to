package com.mx.victorgonzalez.appproyectofinal.Repository

import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostList

interface PostRepository {
    suspend fun getPosts(): PostList
    suspend fun savePost(post: Post?): Post?

}