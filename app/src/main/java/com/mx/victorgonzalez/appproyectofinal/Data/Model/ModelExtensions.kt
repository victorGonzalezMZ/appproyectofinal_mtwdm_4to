package com.mx.victorgonzalez.appproyectofinal.Data.Model

fun List<PostEntity>.toPostList(): PostList {
    val list = mutableListOf<Post>()

    this.forEach{ postEntity ->
        list.add(postEntity.toPost())
    }

    return PostList(list)
}

fun Post.toPostEntity() : PostEntity = PostEntity(
    this.id,
    this.content,
    this.date_posted,
    this.userid
)

fun PostEntity.toPost(): Post = Post(
    this.id,
    this.content,
    this.date_posted,
    this.userid
)
