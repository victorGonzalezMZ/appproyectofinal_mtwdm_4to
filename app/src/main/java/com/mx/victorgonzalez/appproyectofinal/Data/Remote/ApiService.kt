package com.mx.victorgonzalez.appproyectofinal.Data.Remote

import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @GET("posts")
    suspend fun getPosts(): PostList

    @POST("posts")
    suspend fun savePost(@Body post: Post?) : Post?

}