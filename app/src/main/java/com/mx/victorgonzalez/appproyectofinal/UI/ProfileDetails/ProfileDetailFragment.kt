package com.mx.victorgonzalez.appproyectofinal.UI.ProfileDetails

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.UI.Posts.PostsFragmentArgs
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentProfileDetailBinding
import com.squareup.picasso.Picasso

class ProfileDetailFragment : Fragment(R.layout.fragment_profile_detail) {
    private lateinit var binding: FragmentProfileDetailBinding

    private val args by navArgs<PostsFragmentArgs>()

    val db = FirebaseFirestore.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProfileDetailBinding.bind(view)

        db.collection("users")
            .whereEqualTo("username",args.userid)
            .get()
            .addOnSuccessListener { result ->
                for(document in result){
                    binding.textTitleProfile.text = document.getString("username").toString()
                    binding.textContentDescription.text = document.getString("desc").toString()
                    binding.txtDatejoinedProfile.text = document.getString("email").toString()
                    Picasso.get().load(document.getString("profilepic").toString()).into(binding.imgProfile)
                }
            }
            .addOnFailureListener { exception ->
                Log.e("Tag","Fail")
            }

        binding.btnEditProfile.setOnClickListener {
            val action = ProfileDetailFragmentDirections.actionProfileDetailFragmentToProfileEditFragment(
                args.userid
            )
            findNavController().navigate(action)
        }
    }
}