package com.mx.victorgonzalez.appproyectofinal.Data.Model

data class Users(
    val username   : String = "",
    val email      : String = "",
    val desc       : String = "",
    val profilepic : String = "",
    val password   : String = ""
)
