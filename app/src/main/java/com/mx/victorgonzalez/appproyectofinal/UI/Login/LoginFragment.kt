package com.mx.victorgonzalez.appproyectofinal.UI.Login

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentLoginBinding

class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var binding: FragmentLoginBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)

        binding.btnLogin.setOnClickListener {
            if(!binding.etxtUsernameLogin.text.toString().isNullOrEmpty() && !binding.etxtPasswordLogin.text.toString().isNullOrEmpty()){
                val db = FirebaseFirestore.getInstance()

                db.collection("users")
                    .whereEqualTo("username",binding.etxtUsernameLogin.text.toString())
                    .get()
                    .addOnSuccessListener { result ->
                        for(document in result){
                            Log.i("TAG", "Document: " + document)
                            if(binding.etxtPasswordLogin.text.toString().equals(document.getString("password"))){
                                Log.i("Tag", "Document Key: " + document.getString("username"))
                                val action = LoginFragmentDirections.actionLoginFragmentToPostsFragment(
                                    document.getString("username").toString()
                                )
                                findNavController().navigate(action)
                            }
                            else{
                                failedLogin()
                            }
                        }
                    }
                    .addOnFailureListener { exception ->
                        failedLogin()
                    }
            }
            else {
                Snackbar.make(requireView(),R.string.textLoginMissing , Snackbar.LENGTH_LONG).show();
            }
        }

        binding.btnRegister.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToProfileRegisterFragment()
            findNavController().navigate(action)
        }
    }

    private fun failedLogin() {
        Snackbar.make(
            requireView(),
            R.string.textLoginFail.toString(),
            Snackbar.LENGTH_LONG)
            .show();
    }
}