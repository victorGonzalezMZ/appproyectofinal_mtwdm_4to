package com.mx.victorgonzalez.appproyectofinal.Data.Local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostEntity

@Database(entities = [PostEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun PostDao() : PostDao

    companion object{
        private var INSTANCE: AppDatabase? = null

        fun getDataBase(context: Context) : AppDatabase{
            INSTANCE = INSTANCE?: Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "postsapp"
            ).build()

            return INSTANCE!!
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }

}

