package com.mx.victorgonzalez.appproyectofinal.Data.Local

import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostEntity
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostList
import com.mx.victorgonzalez.appproyectofinal.Data.Model.toPostList

class LocalDataSource(private val postDao: PostDao) {
    suspend fun getPosts(): PostList = postDao.getPosts().toPostList()

    suspend fun savePost(post: PostEntity) = postDao.savePost(post)

}