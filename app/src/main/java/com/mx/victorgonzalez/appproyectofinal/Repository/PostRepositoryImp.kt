package com.mx.victorgonzalez.appproyectofinal.Repository

import com.mx.victorgonzalez.appproyectofinal.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostList
import com.mx.victorgonzalez.appproyectofinal.Data.Model.toPostEntity
import com.mx.victorgonzalez.appproyectofinal.Data.Remote.PostDataSource

class PostRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: PostDataSource) : PostRepository {

    override suspend fun getPosts(): PostList {
        dataSource.getPosts().data.forEach{ note ->
            localDataSource.savePost(note.toPostEntity())
        }

        return localDataSource.getPosts()
    }

    override suspend fun savePost(post: Post?): Post? {
        return dataSource.savePost(post)
    }

}