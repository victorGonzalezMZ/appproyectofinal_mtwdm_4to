package com.mx.victorgonzalez.appproyectofinal.Data.Model

data class PostList(
    val data: List<Post> = listOf()
)
