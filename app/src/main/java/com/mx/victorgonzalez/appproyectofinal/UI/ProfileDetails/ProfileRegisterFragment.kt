package com.mx.victorgonzalez.appproyectofinal.UI.ProfileDetails

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Users
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentProfileRegisterBinding

class ProfileRegisterFragment : Fragment(R.layout.fragment_profile_register) {
    private lateinit var binding: FragmentProfileRegisterBinding

    val db = FirebaseFirestore.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProfileRegisterBinding.bind(view)

        binding.btnSaveProfileReg.setOnClickListener {
            binding.textViewUserReg.setTextColor(Color.parseColor("#000000"))
            binding.textViewEReg.setTextColor(Color.parseColor("#000000"))
            binding.textViewPassReg.setTextColor(Color.parseColor("#000000"))
            binding.textViewPassConfReg.setTextColor(Color.parseColor("#000000"))

            if (!binding.etxtUsernameReg.text.toString().isNullOrEmpty() &&
                !binding.etxtEmailReg.text.isNullOrEmpty() &&
                !binding.etxtDescriptionReg.text.isNullOrEmpty() &&
                !binding.etxtPasswordReg.text.isNullOrEmpty() &&
                !binding.etxtPasswordConfReg.text.isNullOrEmpty()){

                if(binding.etxtPasswordReg.text.toString().equals(binding.etxtPasswordConfReg.text.toString())) {


                    db.collection("users")
                        .document()
                        .set(
                            Users(
                                username   = binding.etxtUsernameReg.text.toString(),
                                email      = binding.etxtEmailReg.text.toString(),
                                desc       = binding.etxtDescriptionReg.text.toString(),
                                profilepic = "https://www.nicepng.com/png/detail/128-1280406_view-user-icon-png-user-circle-icon-png.png",
                                password   = binding.etxtPasswordReg.text.toString()
                            ))
                        .addOnSuccessListener {
                            Snackbar.make(requireView(),R.string.textProfileRegSuccess, Snackbar.LENGTH_LONG).show();
                            findNavController().popBackStack()
                        }
                        .addOnFailureListener { exception ->
                            Snackbar.make(
                                requireView(),
                                R.string.textProfileRegError.toString() + " " + exception.message.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        }

                }
                else {
                    Snackbar.make(requireView(),R.string.textProfilePasswordUnconfirm, Snackbar.LENGTH_LONG).show();
                    binding.textViewPassReg.setTextColor(Color.parseColor("#FF0000"))
                    binding.textViewPassConfReg.setTextColor(Color.parseColor("#FF0000"))
                }

            }
            else {
                Snackbar.make(requireView(),R.string.textProfileEditMissing, Snackbar.LENGTH_LONG).show();
                binding.textViewUserReg.setTextColor(Color.parseColor("#FF0000"))
                binding.textViewEReg.setTextColor(Color.parseColor("#FF0000"))
                binding.textViewPassReg.setTextColor(Color.parseColor("#FF0000"))
                binding.textViewPassConfReg.setTextColor(Color.parseColor("#FF0000"))
            }
        }
    }
}