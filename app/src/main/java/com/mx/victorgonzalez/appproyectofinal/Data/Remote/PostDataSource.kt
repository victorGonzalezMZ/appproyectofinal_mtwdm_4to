package com.mx.victorgonzalez.appproyectofinal.Data.Remote

import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Data.Model.PostList

class PostDataSource(private val apiService: ApiService) {

    suspend fun getPosts(): PostList = apiService.getPosts()

    suspend fun savePost(post: Post?) : Post? = apiService.savePost(post)

}