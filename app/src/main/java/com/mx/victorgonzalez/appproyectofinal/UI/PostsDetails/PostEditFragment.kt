package com.mx.victorgonzalez.appproyectofinal.UI.PostsDetails

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.firestore.FirebaseFirestore
import com.mx.victorgonzalez.appproyectofinal.Core.Resource
import com.mx.victorgonzalez.appproyectofinal.Data.Local.AppDatabase
import com.mx.victorgonzalez.appproyectofinal.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appproyectofinal.Data.Model.Post
import com.mx.victorgonzalez.appproyectofinal.Data.Remote.ApiClient
import com.mx.victorgonzalez.appproyectofinal.Data.Remote.PostDataSource
import com.mx.victorgonzalez.appproyectofinal.Presentation.PostViewModel
import com.mx.victorgonzalez.appproyectofinal.Presentation.PostViewModelFactory
import com.mx.victorgonzalez.appproyectofinal.R
import com.mx.victorgonzalez.appproyectofinal.Repository.PostRepositoryImp
import com.mx.victorgonzalez.appproyectofinal.UI.Posts.PostsFragmentArgs
import com.mx.victorgonzalez.appproyectofinal.databinding.FragmentPostEditBinding
import java.text.SimpleDateFormat
import java.util.*

class PostEditFragment : Fragment(R.layout.fragment_post_edit) {
    private lateinit var binding: FragmentPostEditBinding

    private val args by navArgs<PostsFragmentArgs>()

    private val viewModel by viewModels<PostViewModel> {
        PostViewModelFactory(
            PostRepositoryImp(
                LocalDataSource(AppDatabase.getDataBase(this.requireContext()).PostDao()),
                PostDataSource(ApiClient.service)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPostEditBinding.bind(view)

        binding.btnSavePostEdit.setOnClickListener {
            if(!binding.etxtContentPost.text.isNullOrEmpty()){
                val date = getCurrentDateTime()
                val dateInString = date.toString("yyyy/MM/dd HH:mm:ss")

                var post = Post(
                    0,
                    binding.etxtContentPost.text.toString(),
                    dateInString,
                    args.userid
                )

                Log.i("VISE", "Post: " + post)

                viewModel.savePost(post).observe(viewLifecycleOwner, { result ->
                    when (result) {
                        is Resource.Loading -> {
                            binding.progressbarPostEdit.visibility = View.VISIBLE
                            Log.d("LiveData", "Loading data...")
                        }
                        is Resource.Success -> {
                            binding.progressbarPostEdit.visibility = View.GONE
                            findNavController().popBackStack()
                        }
                        is Resource.Failure -> {
                            binding.progressbarPostEdit.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "${result.exception.toString()}",
                                Toast.LENGTH_LONG
                            ).show()
                            Log.d("LiveData", "${result.exception.toString()}")
                        }
                    }
                })
            }
        }
    }

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }
}